def reverser
  result = []
  words = yield.split(' ')
  words.each do |word|
    result.push(word.reverse)
  end
  result.join(' ')
end


def adder (num = 1)
  yield + num
end


def repeater (num = 0)
  if num == 0
    return yield
  else
    num.times do |n|
      yield
    end
  end
end
